<!DOCTYPE HTML>
<html>

	<head>
	
		<title>Just Delete Me | A directory of direct links to delete your account from web services.</title>
		
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		
		<link rel="apple-touch-icon-precomposed" href="../assets/icons/apple-touch-icon-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/icons/apple-touch-icon-120x120-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/icons/apple-touch-icon-144x144-precomposed.png">
		
		<link rel="shortcut icon" href="../assets/icons/favicon.ico">
		<link rel="stylesheet" type="text/css" href="../assets/css/style.css" />
		<link type="text/css" rel="stylesheet" href="../assets/css/libs/jquery.dropdown.css" />
		
			
	</head>
	
	<body lang="en">

	<?php include('../parts/nav.php'); ?>
		
		<nav>
			<!-- begin language switcher -->
			<span class="language-switch" href="#" data-dropdown="#dropdown-1" id="en">English</span>
			<!-- end language switcher -->
			<a href="#about" class="info">About</a>
			<a href="./fake-identity-generator" class="id-gen">Fake Identity Generator</a>
		</nav>
		
		<header>
			<h1>just<span>delete</span>.me</h1>
			<p class="tagline">A directory of direct links to delete your account from web services.</p>

			<p>
				<a href="#extension" class="button banner">Chrome Extension</a>

				<a class="button" href="http://github.com/rmlewisuk/justdelete.me">Fork&nbsp;on&nbsp;GitHub</a>

				<a class="button" href="https://twitter.com/intent/tweet?text=Just+Delete+Me+-+A+directory+of+direct+links+to+delete+your+account+from+web+services.+http%3A%2F%2Fjustdelete.me">Tweet JDM</a>
			</p>
		</header>
		
		<section class="main">
		
			<div class="generate">
				<h2>Fake Identity Generator</h2>
				<p>Generate a fake name, address, date of birth, username, password and biography.</p>
				<button class="generate">Generate fake identity</button><br/>
				<input type="radio" name="gender" id="male" checked> Male <input type="radio" name="gender"> Female
			</div>
		    <div id="identity"></div>

		    <div class="tweet-button">
		    	<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://justdelete.me/fake-identity-generator" data-text="I created a fake identity with JustDelete.me!" data-size="large" data-count="none">Tweet</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
		    </div>
			
		</section>
		
		<?php include('../parts/footer.php') ?>

<script src="../assets/js/libs/jquery.js"></script>
<script src="../assets/js/libs/jquery.dropdown.js"></script>
<script src="../assets/js/scripts.js"></script>
<script src="../assets/js/gen/generate.js"></script>
<script src="../assets/js/gen/markov.min.js"></script>
		
	</body>
	
</html>