<?php
$fname = $_POST['fname'];
$lname = $_POST['lname'];
?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <title>
                                Your Background Search Results for <?=$fname?> <?=$lname?>| Background Checks.org    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="robots" content="noindex, nofollow">
    
<meta property="og:type" content="article" />
<meta property="og:url" content="http://backgroundchecks.org/?page_id=3082" />
<meta name="twitter:card" content="summary_large_image" />
<meta property="og:site_name" content="BackgroundChecks.org" />
<meta property="og:title" content="Results" />
<meta name="twitter:title" content="Results" />
<meta name="twitter:creator" content="@backchecksorg" />
<meta name="twitter:site" content="@backchecksorg" />

<link href="//fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" /><link href="//fonts.googleapis.com/css? family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" /><link href="//fonts.googleapis.com/css?family=Raleway:300,600"  rel="stylesheet" type="text/css" />   
		
<link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic %2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://backgroundchecks.org/wp-includes/css/dashicons.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://backgroundchecks.org/wp-includes/css/admin-bar.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='tve_landing_page_vibrant_video_sales_page-css'  href='http://backgroundchecks.org/wp-content/plugins/thrive-visual- editor/landing-page/templates/css/vibrant_video_sales_page.css?ver=1.95' type='text/css' media='all' />
<link rel='stylesheet' id='tve_default-css'  href='http://backgroundchecks.org/wp-content/plugins/thrive-visual-editor/editor/css/thrive_default.css? ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='tve_colors-css'  href='http://backgroundchecks.org/wp-content/plugins/thrive-visual-editor/editor/css/thrive_colors.css?ver=4.3.1'  type='text/css' media='all' />
<link rel='stylesheet' id='tve_style_family_tve_flt-css'  href='http://backgroundchecks.org/wp-content/plugins/thrive-visual- editor/editor/css/thrive_flat.css?ver=1.95' type='text/css' media='all' />
<link rel='stylesheet' id='focusblog-style-css'  href='http://backgroundchecks.org/wp-content/themes/focusblog/style.css?ver=4.3.1' type='text/css'  media='all' />
<link rel='stylesheet' id='thrive-reset-css'  href='http://backgroundchecks.org/wp-content/themes/focusblog/css/reset.css?ver=20120208' type='text/css'  media='all' />
<link rel='stylesheet' id='thrive-main-style-css'  href='http://backgroundchecks.org/wp-content/themes/focusblog/css/main_green.css?ver=5566' type='text/css'  media='all' />
        <script type="text/javascript">
            function w3tc_popupadmin_bar(url) {
                return window.open(url, '', 'width=800,height=600,status=no,toolbar=no,menubar=no,scrollbars=yes');
            }
        </script>
            <script type='text/javascript' src='http://backgroundchecks.org/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='http://backgroundchecks.org/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='http://backgroundchecks.org/wp-content/plugins/thrive-visual-editor/editor/js/compat.min.js?ver=1.95'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://backgroundchecks.org/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://backgroundchecks.org/wp-includes/wlwmanifest.xml" /> 

    <style type="text/css">.thrv_page_section .out {max-width: none}</style>

</head>
<body class="page page-id-3082 page-template-default  no-customize-support tve_lp" style="">
<div class="wrp cnt bSe" style="display: none">
    <div class="awr"></div>
</div>
<div class="tve_wrap_all" id="tcb_landing_page" style="padding: 1px 0 0">
    <div class="tve_post_lp tve_lp_vibrant_video_sales_page tve_lp_template_wrapper" style="">
        <div id="tve_flt" class="tve_flt"><div id="tve_editor" class="tve_shortcode_editor"><div class="tve_lp_header tve_empty_dropzone tve_content_width">
</div>
<div class="tve_lp_content tve_editor_main_content tve_empty_dropzone thrv_wrapper tve_no_drag" style="margin-top: 50px; max-width: 1180px; background- attachment: scroll;">
<h1 class="tve_p_left" style="font-size: 56px; margin-bottom: 40px; margin-top: 0px; color: rgb(21, 21, 21);">
Background Check Results for: <font color="#e60e0e"><span class="underline_text"><?=$fname?> <?=$lname?></span></font></h1>
<h4 class="tve_p_center bold_text"><font color="#f58000"><b>Congratulations! &nbsp;</b></font>We have successfully located results for your search among  several trusted background check sites. Please choose one below to see the<span class="bold_text"> full results.&nbsp;</span></h4><div class="thrv_wrapper  thrv_columns">
<div class="tve_colm tve_twc"><p class="">Website: TruthFinder.com</p><div style="width: 152px" class="thrv_wrapper tve_image_caption aligncenter">
<span class="tve_image_frame">
<img class="tve_image" alt="" src="http://backgroundchecks.org/wp-content/uploads/2016/08/logo-truthFinders.png" style="width: 152px">
</span>
</div><div class="thrv_wrapper thrv_star_rating">
<span class="tve_rating_stars tve_style_star" data-value="9.6" data-max="10" title="9.6 / 10" style="width: 240px;"><span style="width:  230.4px;"></span></span>
</div><div class="thrv_wrapper thrv_button_shortcode" data-tve-style="1">
<div class="tve_btn tve_nb tve_bigBtn tve_red tve_btn1">
<a href="" class="tve_btnLink" target="_blank">
<span class="tve_left tve_btn_im">
<i></i>
<span class="tve_btn_divider"></span>
</span>
<span class="tve_btn_txt">Visit TruthFinder.com</span>
</a>
</div>
</div><p class="">?</p></div>
<div class="tve_colm tve_twc tve_lst"><p class="">Offers a Trial Membership? Yes. $1 for 5 days. Not always available.</p><p class=""><span  class="bold_text">What we love: </span>Stunning data accuracy, reasonable pricing and a nice interface make this our #1 pick.</p><p class=""><span  class="bold_text">What we wish was better: </span>Faster interface, consistent availability of the free trial.&nbsp;</p><p class="">Editor's Summary:?Truth  Finder is a newer website in the background check space, but seems to offer the best data and pricing right now. They don't always offer a free trial, which  we would like to see, but overall the pricing was very competitive and the information was top notch, and that's what makes it our current #1  recommendation.</p><div class="thrv_wrapper thrv_button_shortcode" data-tve-style="1">
<div class="tve_btn tve_nb tve_bigBtn tve_red tve_btn1">
<a href="" class="tve_btnLink" target="_blank">
<span class="tve_left tve_btn_im">
<i></i>
<span class="tve_btn_divider"></span>
</span>
<span class="tve_btn_txt">Visit TruthFinder.com</span>
</a>
</div>
</div><p class=""></p></div>
</div><div class="thrv_wrapper thrv_columns">
<div class="tve_colm tve_twc"><p class="">Website: Instant Checkmate</p><div style="width: 152px" class="thrv_wrapper tve_image_caption aligncenter">
<span class="tve_image_frame">
<img class="tve_image" alt="" src="http://backgroundchecks.org/wp-content/uploads/2016/08/logo-icm.png" style="width: 152px">
</span>
</div><div class="thrv_wrapper thrv_star_rating">
<span class="tve_rating_stars tve_style_star" data-value="9" data-max="10" title="9 / 10" style="width: 240px;"><span style="width: 216px;"></span></span>
</div><div class="thrv_wrapper thrv_button_shortcode" data-tve-style="1">
<div class="tve_btn tve_nb tve_bigBtn tve_red tve_btn1">
<a href="" class="tve_btnLink" target="_blank">
<span class="tve_left tve_btn_im">
<i></i>
<span class="tve_btn_divider"></span>
</span>
<span class="tve_btn_txt">Visit Instant Checkmate</span>
</a>
</div>
</div><p class=""></p></div>
<div class="tve_colm tve_twc tve_lst"><p class="">Offers a Trial Membership? Yes, $1 for 5 days.</p><p class=""><span class="bold_text">What we love:  </span>Amazing support, best pricing, solid company that's been in business for a long time.</p><p class="bold_text"><span class="bold_text">What we wish was  better: </span>&nbsp;The data at Instant Checkmate is solid, but we wish they updated as fast as TruthFinder.com (the difference is not huge, just a nice do  have). Also, we're not huge fans of charging $1.99 for a PDF. Just print from your computer.</p><p class="">?</p><p class=""><span class="bold_text">Editor's  Summary</span>:Instant Checkmate is the industry leader in a lot of ways. The data is consistent, the support is phenomenal, and the pricing is great too.  Long considered our #1 choice, they've to #2 as upstart TruthFinder.com has taken the lead. That being said we still solidly recommend them and you'll get a  great interface and data from them.</p><div class="thrv_wrapper thrv_button_shortcode" data-tve-style="1">
<div class="tve_btn tve_nb tve_bigBtn tve_red tve_btn1">
<a href="" class="tve_btnLink" target="_blank">
<span class="tve_left tve_btn_im">
<i></i>
<span class="tve_btn_divider"></span>
</span>
<span class="tve_btn_txt">Visit Instant Checkmate</span>
</a>
</div>
</div><p class=""></p></div>
</div><div class="thrv_wrapper thrv_columns">
<div class="tve_colm tve_twc"><p class="">Website: Been Verified</p><div style="width: 152px" class="thrv_wrapper tve_image_caption aligncenter">
<span class="tve_image_frame">
<img class="tve_image" alt="" src="http://backgroundchecks.org/wp-content/uploads/2016/08/logo-beenVerified.jpg" style="width: 152px">
</span>
</div><div class="thrv_wrapper thrv_star_rating">
<span class="tve_rating_stars tve_style_star" data-value="8.6" data-max="10" title="8.6 / 10" style="width: 240px;"><span style="width:  206.4px;"></span></span>
</div><div class="thrv_wrapper thrv_button_shortcode" data-tve-style="1">
<div class="tve_btn tve_nb tve_bigBtn tve_red tve_btn1">
<a href="" class="tve_btnLink" target="_blank">
<span class="tve_left tve_btn_im">
<i></i>
<span class="tve_btn_divider"></span>
</span>
<span class="tve_btn_txt">Visit BeenVerified</span>
</a>
</div>
</div><p class=""></p></div>
<div class="tve_colm tve_twc tve_lst"><p class="">Offers a Trial Membership? Yes, $1 for 5 days.</p><p class=""><span class="bold_text">What we love:  </span>Their social media data and accuracy is downright scary! (In a good way).</p><p class="bold_text"><span class="bold_text">?What we wish was better:  </span>&nbsp;The Social Data (including pictures) is solid, and the criminal data is really good - but we just wish their interface was a little  cleaner.</p><p class="">?</p><p class=""><span class="bold_text">Editor's Summary: </span>BeenVerified has been around forever, and they have cheaper pricing  then a lot of their competitors. We really love their social media search (our picture showed up when we searched!), but found that some of their criminal  and public records were lacking compared to TruthFinder and Instant Checkmate. Still a great service, and our #3 choice. If you are particularly interested  in online data, they're your best bet.</p><div class="thrv_wrapper thrv_button_shortcode" data-tve-style="1">
<div class="tve_btn tve_nb tve_bigBtn tve_red tve_btn1">
<a href="" class="tve_btnLink" target="_blank">
<span class="tve_left tve_btn_im">
<i></i>
<span class="tve_btn_divider"></span>
</span>
<span class="tve_btn_txt">Visit BeenVerified</span>
</a>
</div>
</div><p class="">?</p></div>
</div>
</div>
<div class="tve_lp_footer tve_empty_dropzone tve_drop_constraint" data-forbid=".thrv_page_section,.sc_page_section">
</div></div></div>    </div>
</div>

</script>
<script type='text/javascript' src='http://backgroundchecks.org/wp-content/themes/focusblog/js/script.min.js?ver=4.3.1'></script>
	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>

		</body>
</html>