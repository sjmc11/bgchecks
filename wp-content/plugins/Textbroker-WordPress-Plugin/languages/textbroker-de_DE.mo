��    l      |  �   �      0	     1	     8	  	   @	     J	  	   Q	  '   [	  %   �	  '   �	  
   �	     �	     �	  
   �	     
     
     
     $
     2
     B
     R
     ^
     k
     z
     �
     �
     �
  &   �
     �
  !     0   (  9   Y     �  !   �  (   �  )   �  $   '  5   L     �     �     �     �     �     �     �               +     ?     D  	   S     ]     m  	   r  
   |  	   �     �     �  
   �     �     �     �          $  %   ?     e  !   ~  
   �     �     �     �  
   �     �     �     �     �     �     �           	               +     2     C     J     S     \     e     n     w     ~     �     �     �     �     �     �     �     �            
   (     3     A  ;   O     �     �     �  �  �     I     R     [     i     o     r     �      �  
   �     �     �               (  
   0     ;     K  (   b     �     �     �     �     �     �        ,        >     P  ?   p  T   �  +     %   1  *   W     �  (   �  -   �  
   �     �          #     7  2   I  -   |     �  &   �     �     �     �                :     ?  	  M  ,   W     �  	   �     �     �     �  +   �  $        1  *   O     z  *   �     �     �  	   �     �                               .     >     G     P     Y     b     x     �  	   �  2   �     �     �     �     �          !     )     8     @     N     ]     d     q     �     �     �     �     �     �  �   �     �     �     �         ,   P      I      U   V      9   Y   W             B   4   d   D               X   *   e       "   i           >   @             g      	   =                  7   :       5   f   ^       l   #   +   [   K      '   F           8   H         ]   
          ?   J          .              ;      A   !         O   <   `   E   )   1                    T   _   Z   2      a   &       3   h       $       j           c       M       N       -   L   6   G      k   /                    b   C                       (      S           Q   %       R              0   \    Accept Actions Add order Author Budget id Budget information deleted successfully Budget information saved successfully Budget information updated successfully Budget key Budget left Budget locked Budget max Budget used Budgets Classification Cost per word Cost textbroker Could not login Count words Create order Current status Delete Due days ERROR: Budget id is invalid ERROR: Budget id is missing ERROR: Budget information is incorrect ERROR: Budget key is missing ERROR: Budget password is missing ERROR: Due days have to be set to 1 or more days ERROR: Maximum words cannot be smaller than minimum words ERROR: Rating has to be chosen ERROR: Words minimum is 100 words ERROR: You have to provide an order text ERROR: You have to provide an order title ERROR: You have to select a category ERROR: Your feedback has to be at least 50 characters Edit Enter budget id Enter budget information Enter budget key Enter budget password Failed to publish page Failed to publish post Handling time Klick to refresh status List budget details Live Manage budgets Max. cost Max. cost words Name New order No budgets No orders Order accepted successfully Order category Order date Order deleted successfully Order id Order published successfully Order rejected successfully Order removed successfully Order revision requested successfully Order saved successfully Order status updated successfully Order text Order title Orders Page Period end Period start Post Preview Publish Rating Rating 2 Rating 3 Rating 4 Rating 5 Re-create order Remove Request revision Review Review 0 Review 1 Review 2 Review 3 Review 4 Revise Sandbox Save budget information Service location Service: de Service: us Status Submenu: Check Submenu: Order Textbroker-WordPress-Plugin Title Update budget information Word count Words maximum Words minimum You find your budget information on the Textbroker website. Your comment day days Project-Id-Version: Textbroker WordPress Plugin 1.5
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-02-13 12:52+0200
PO-Revision-Date: 
Last-Translator: Fabio Bacigalupo <fabio.bacigalupo@gmail.com>
Language-Team: Fabio Bacigalupo <info1@open-haus.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
 Annehmen Aktionen Neuer Auftrag Autor Id Budget erfolgreich gelöscht. Budget erfolgreich gespeichert. Budget erfolgreich aktualisiert. Budget Key Verbleibendes Budget Gesperrtes Budget Maximales Budget Verbrauchtes Budget Budgets Einstufung Kosten pro Wort TB-Bearbeitungsgebühr Login konnte nicht durchgeführt werden. Anzahl der Wörter Auftrag erteilen Aktueller Status Löschen Bearbeitungszeit in Tagen Budget ID ist nicht gültig. Budget ID fehlt. Die Budget-Informationen sind nicht korrekt. Budget Key fehlt. Passwort für das Budget fehlt. Die Bearbeitungszeit muss zwischen einem und zehn Tagen liegen. Die maximale Anzahl an Wörtern muss größer oder gleich der minimalen Anzahl sein. Die Qualitätsstufe muss festgelegt werden. Die Mindestanzahl beträgt ein Wort.  Die Auftragsbeschreibung (Briefing) fehlt. Der Auftragstitel fehlt. Es muss eine Kategorie ausgewählt sein. Das Feedback muss mind. 50 Zeichen lang sein. Bearbeiten Budget-ID eingeben Budget hinzufügen Budget-Key eingeben Passwort eingeben Auftrag konnte nicht als Seite gespeichert werden. Auftrag konnte nicht als Artikel gespeichert. Bearbeitungszeit Anklicken zum Aktualisieren des Status Budget-Details anzeigen Live Auftrag erteilen Maximale Kosten Max. Kosten aller Wörter Name Neuer Auftrag Sie haben noch keine Budgets angelegt. Sie benötigen mindestens ein Budget, um Aufträge erstellen zu können. Fügen Sie jetzt ein Budget hinzu. Die Budget-Daten finden Sie im <a href="http://www.textbroker.de/c/api.php">API-Bereich Ihres Textbroker-Accounts</a>. Es liegen keine gespeicherten Aufträge vor. Text erfolgreich angenommen. Kategorie Bestelldatum Auftrag erfolgreich gelöscht. Order-ID Auftrag erfolgreich ins System übernommen. Auftrag erfolgreich zurückgewiesen. Eintrag erfolgreich entfernt. Änderungswunsch erfolgreich übermittelt. Auftrag erfolgreich erteilt. Der Status wurde erfolgreich aktualisiert. Auftragsbeschreibung (Briefing) Auftragstitel Aufträge Seite Ende Beginn Artikel Vorschau Veröffentlichen Qualitätsstufe 2 Sterne 3 Sterne 4 Sterne 5 Sterne Auftrag neu erstellen Eintrag entfernen Überarbeitung anfordern Bewertung keine Bewertung abgeben, aber den Artikel annehmen super gut geht so nicht so besonders Änderungswunsch einreichen Sandbox Budget anlegen Service textbroker.de textbroker.com Status Budget-Check Auftragsverwaltung Textbroker WordPress-Plugin Titel Budget aktualisieren Max. Wortanzahl Anzahl Worte Maximum Anzahl Worte Minimum Sie müssen zuerst ein Budget über die Textbroker-Webseite anlegen. Als eingeloggter Benutzer finden Sie im Kundenbereich einen Abschnitt, der den Namen "API" trägt. Dort finden Sie alle weiteren Informationen zum Umgang mit Budgets und der API. Ihr Feedback Tag Tage 