��    l      |  �   �      0	     1	     8	  	   @	     J	  	   Q	  '   [	  %   �	  '   �	  
   �	     �	     �	  
   �	     
     
     
     $
     2
     B
     R
     ^
     k
     z
     �
     �
     �
  &   �
     �
  !     0   (  9   Y     �  !   �  (   �  )   �  $   '  5   L     �     �     �     �     �     �     �               +     ?     D  	   S     ]     m  	   r  
   |  	   �     �     �  
   �     �     �     �          $  %   ?     e  !   ~  
   �     �     �     �  
   �     �     �     �     �     �     �           	               +     2     C     J     S     \     e     n     w     ~     �     �     �     �     �     �     �     �            
   (     3     A  ;   O     �     �     �  �  �     I     P  	   X     b  	   i  '   s  %   �  '   �  
   �     �               "     .     6     E     S  (   Z     �     �     �     �     �     �     �  (   �          '  ,   C  9   p  ,   �  ,   �  "     #   '     K  A   j     �     �  
   �     �     �     �     
     !     /     G     [  	   `  	   j     t     �  	   �     �  	   �     �     �  
   �     �               4     Q  %   l     �  !   �  
   �     �     �     �     �     �     �     �                         &     .     6     I     V     g      n  	   �     �  
   �     �     �     �     �     �     �     �     �                    1     7  
   Q     \     j  �   x     -     :     >         ,   P      I      U   V      9   Y   W             B   4   d   D               X   *   e       "   i           >   @             g      	   =                  7   :       5   f   ^       l   #   +   [   K      '   F           8   H         ]   
          ?   J          .              ;      A   !         O   <   `   E   )   1                    T   _   Z   2      a   &       3   h       $       j           c       M       N       -   L   6   G      k   /                    b   C                       (      S           Q   %       R              0   \    Accept Actions Add order Author Budget id Budget information deleted successfully Budget information saved successfully Budget information updated successfully Budget key Budget left Budget locked Budget max Budget used Budgets Classification Cost per word Cost textbroker Could not login Count words Create order Current status Delete Due days ERROR: Budget id is invalid ERROR: Budget id is missing ERROR: Budget information is incorrect ERROR: Budget key is missing ERROR: Budget password is missing ERROR: Due days have to be set to 1 or more days ERROR: Maximum words cannot be smaller than minimum words ERROR: Rating has to be chosen ERROR: Words minimum is 100 words ERROR: You have to provide an order text ERROR: You have to provide an order title ERROR: You have to select a category ERROR: Your feedback has to be at least 50 characters Edit Enter budget id Enter budget information Enter budget key Enter budget password Failed to publish page Failed to publish post Handling time Klick to refresh status List budget details Live Manage budgets Max. cost Max. cost words Name New order No budgets No orders Order accepted successfully Order category Order date Order deleted successfully Order id Order published successfully Order rejected successfully Order removed successfully Order revision requested successfully Order saved successfully Order status updated successfully Order text Order title Orders Page Period end Period start Post Preview Publish Rating Rating 2 Rating 3 Rating 4 Rating 5 Re-create order Remove Request revision Review Review 0 Review 1 Review 2 Review 3 Review 4 Revise Sandbox Save budget information Service location Service: de Service: us Status Submenu: Check Submenu: Order Textbroker-WordPress-Plugin Title Update budget information Word count Words maximum Words minimum You find your budget information on the Textbroker website. Your comment day days Project-Id-Version: Textbroker Wordpress Plugin 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-02-13 12:52+0200
PO-Revision-Date: 
Last-Translator: Fabio Bacigalupo <fabio.bacigalupo@gmail.com>
Language-Team: Fabio Bacigalupo <info1@open-haus.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
 Accept Actions Add order Author Budget id Budget information deleted successfully Budget information saved successfully Budget information updated successfully Budget key Remaining budget Locked budget Maximal budget Used budget Budgets Classification Cost per word TB fee Login failed. Is the data given correct? Count words Create order Current status Delete Due days Budget ID is invalid. Budget ID is missing. The given budget information is invalid. Budget key is missing. Budget password is missing. Due days has to be a value between 1 and 10. Words maximum has to be equal or more than words minimum. You have to choose the rating of your order. You have to set the minimum amount of words. You have to provide an order text. You have to provide an order title. You have to select a category. Please give a feedback with at least 50 characters to the author. Edit Enter budget id Add budget Enter budget key Enter budget password Failed to publish page Failed to publish post Handling time Klick to refresh status List budget details Live Add order Max. cost Max. cost words Name New order You have not added any budgets. No orders Order accepted successfully Order category Order date Order deleted successfully Order id Order published successfully Order successfully rejected. Entry removed successfully Order revision requested successfully Order saved successfully Order status updated successfully Order text Order title Orders Page End Start Post Preview Publish Rating 2 Stars 3 Stars 4 Stars 5 Stars Create order again Remove entry Request revision Rating no rating but accept the article excellent good acceptable poor Revise Sandbox Save budget information Service textbroker.de textbroker.com Status Budget check Orders Textbroker WordPress-Plugin Title Update budget information Max. words Words maximum Words minimum You first need to add a budget on the Textbroker website. As logged in user look for the section called "API". There you will find further instructions on how to work with budgets. Your comment day days 