=== Plugin Name ===
Contributors: OpenHaus
Tags: text, marketplace, Textbroker, API, content, SEO
Requires at least: 3.0.1
Tested up to: 3.1
Stable tag: 2.0

Manage your orders for the Textbroker marketplace and publish delivered content right through the backend.

== Description ==

Handle all your content ordering on the Textbroker marketplace through your WordPress backend. Create orders, update their status, ask for revisions. You can work with one or multiple budgets.

This WordPress plugin is an official piece of software by Sario Marketing GmbH / Textbroker International, LLC released under the GPL.


== Installation ==

1. Get an account for [Textbroker][TextBroker] ([German site][TextBrokerDe])
2. Activate the API and create a budget in your Textbroker account
3. Upload `Textbroker-WordPress-Plugin.php` to the `/wp-content/plugins/` directory
4. Activate the plugin through the 'Plugins' menu in WordPress
5. Enter the budget information through the Wordpress backend

== Frequently Asked Questions ==

= Does this plugin cost money? =

No, this piece of software is delivered for free.

= Is this the official WordPress plugin by Textbroker? =

Yes, this is the official and approved WordPress plugin by Textbroker.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the directory of the stable readme.txt, so in this case, `/tags/4.3/screenshot-1.png` (or jpg, jpeg, gif)
2. This is the second screen shot

== Changelog ==

= 2.0 =

* Initial public release by Textbroker under the GPL

== A note by the author ==

If you are interested in Open Source software please have a look at our sites [open haus][OpenHaus] (in German) and the [Linux Showroom][LinuxShowroom] (in English).


[TextBroker]: http://www.textbroker.com
            "Textbroker.com - International site"

[TextBrokerDe]: http://www.textbroker.de
            "Textbroker.de - German site"

[OpenHaus]: http://www.open-haus.de
            "Informationen, Tipps und Tricks zu Open Source-Lösungen, GNU/Linux und das offene Web"

[LinuxShowroom]: http://www.linux-showroom.com
            "Information, tips and tricks on Open Source software, GNU/Linux and other interesting stuff for serious webmasters"