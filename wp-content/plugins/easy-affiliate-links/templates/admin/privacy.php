<?php
/**
 * Template for the privacy policy.
 *
 * @link       http://bootstrapped.ventures
 * @since      2.6.2
 *
 * @package    Easy_Affiliate_Links
 * @subpackage Easy_Affiliate_Links/templates/admin
 */

?>
<h2><?php _e( 'What personal data we collect and why we collect it' ); ?></h2>
<h3><?php _e( 'Analytics' ); ?></h3>
<?php _e( 'When enabled, we track affiliate link clicks for analytic purposes. These include a (optionally hashed) IP address and user ID (when logged in).', 'easy-affiliate-links' ); ?>
<h2><?php _e( 'How long we retain your data' ); ?></h2>
<?php _e( 'Click information is retained in the local database indefinitely for analytic purposes and optional export.', 'easy-affiliate-links' ); ?>
