<?php
// Start the engine
require_once(TEMPLATEPATH.'/lib/init.php');

// Remove breadcrumbs on home page
add_action('get_header', 'remove_breadcrumbs');
function remove_breadcrumbs() {
    if(is_front_page())
    remove_action('genesis_before_loop', 'genesis_do_breadcrumbs'); 
}  

// Add new image sizes
add_image_size('sidebar-alt', 125, 125, TRUE);