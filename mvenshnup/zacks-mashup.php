<?

$ticker = 'AAPL';
$api_key ='.json?api_key=H-gDVVC8MxMhzs9DHVNu';



function analyze($zack_url,$ticker,$api_key){

$url = $zack_url .$ticker .$api_key;
$data = json_decode(file_get_contents($url),true);
return $data;

}

$zack_eps = analyze('https://www.quandl.com/api/v3/datasets/ZEE/', $ticker .'_Q', $api_key);
$zack_zss = analyze('https://www.quandl.com/api/v3/datasets/ZSS/', $ticker, $api_key);
$zack_zea  = analyze('https://www.quandl.com/api/v3/datasets/ZEA/', $ticker, $api_key);
$zack_zar  = analyze('https://www.quandl.com/api/v3/datasets/ZAR/', $ticker, $api_key);

var_dump($zack_eps);

$quarter1 = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][4]));
$quarter2 = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][5]));
$fy = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][6]));



#echo $quarter1;
#echo $zack_eps['dataset']['data'][0][1];

$title = "Analysts Expecting  $ {$zack_eps[dataset][data][0][1]} Earnings Per Share for $ticker this coming quarter";
echo $title;

$content = <<<EX
Wall Street Analysts have given a mean estimate of $ {$zack_eps[dataset][data][0][1]} earnings per share to {$company}. This figure is for {$ticker}'s current quarter. The data was received from
Wall Street Sell Side analysts who maintain coverage on $company. Zack's Research compiled the data from the brokerage firms who currently cover the stock. For this estimate, a total of {$zack_eps[dataset][data][0][5]}
analysts were surveyed.
<br />

Among the analysts, the highest expected of earnings per a share was $ {$zack_eps[dataset][data][0][3]}, and the lowest is $ {$zack_eps[dataset][data][0][4]}.
<br />

In its most recent quarter $company had actual sales of $ {$zack_zss[dataset][data][0][8]}. Among the {$zack_zss[dataset][data][0][13]} analysts who were surveyed, the consensus expectation for quarterly sales had been
{$zack_zss[dataset][data][0][10]}. This represents a {$zack_zss[dataset][data][0][12]}% difference between analyst expectations and the $company achieved in its quarterly earnings.

We've also learned that $company will report its next earnings on {$quarter1}. The earnings report after that one will be on {$quarter2}, and the report for the fiscal year will be made available on {$fy}.
<br>
Last quarters actual earnings were {$zack_zea['dataset']['data'][0][10]} per share.
<br />
<h2> Analysts continue to rate $ticker </h2>

In other news, sell side brokers and analysts continue to rate {$company}:

<ul>
<li>{$zack_zar['dataset']['data'][0][2]} Analysts rate the company a <strong>strong buy</strong></li>
<li>{$zack_zar['dataset']['data'][0][5]}  Analysts rate the company a <strong>buy</strong> </li>
<li>{$zack_zar['dataset']['data'][0][9]}  Analysts rate the company a <strong>hold</strong></li>

<li>{$zack_zar['dataset']['data'][0][13]}  Analysts rate the company a <strong>sell</strong></li>
<li>{$zack_zar['dataset']['data'][0][17]}  Analysts rate the company a <strong>strong sell</strong></li>
</ul>

The overall rating for the company is {$zack_zar['dataset']['data'][0][21]}. The rating is an average of the various different ratings given by analysts and brokers, and then averaged into one rating by the good people at Zacks Research in Chicago, IL.
For $company, the numerical average rating system is as follows:
<ol>
<li><strong>Strong buy</strong> for $ticker</li>
<li><strong>BUY</strong> for $ticker</li>
<li><strong>HOLD</strong> for $ticker</li>

<li><strong>SELL</strong> for $ticker</li>
<li><strong>SELL</strong> for $ticker</li>
</ol>




EX;

echo $content;

?>





