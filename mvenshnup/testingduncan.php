<?
/***
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
****/

include('IXR_Library.php');
$usr = 'Jon';
$pwd = '@u1UX4HgUpLfqVCPlB9CuplK';
$xmlrpc = 'http://www.duncanindependent.com/xmlrpc.php';
$client = new IXR_Client($xmlrpc);
$client -> debug = true; //optional but useful
$servername = "45.55.38.170";
$username = "root";
$password = "Wh1LF1Nd0i";
$dbname = "finance_publisher";
$api_key ='.json?api_key=H-gDVVC8MxMhzs9DHVNu';



function analyze($zack_url,$ticker,$api_key){

$url = $zack_url .$ticker .$api_key;
$data = json_decode(file_get_contents($url),true);
return $data;

}


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

#this is for top 1800 stocks
$sql = "SELECT * from top_tickers " ;
#$sql = "SELECT * from stocks where exchange =" ."'NYSE'"  ."limit 2500";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {


$query = "SELECT dataset_code,description from blurbs WHERE ticker = '$row[ticker]' ";
$query_result = $conn->query($query);
while($data = $query_result->fetch_assoc())

{
$short_query = "SELECT * from shortdata WHERE SYMBOL = '$row[ticker]' ";
$short_query_result = $conn->query($short_query);
while($shortdata = $short_query_result->fetch_assoc()){
$blurb =  $data['description'];
$ticker = $row['ticker'];
$yahoourl = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20%28%22' .$ticker  .'%22%29&format=json&env=http://datatables.org/alltables.env';
$imgurl = 'http://chart.finance.yahoo.com/z?s=' .$ticker .'&t=6m&q=l&z=l';
#error with yahoo needs fixing
$yahoo = json_decode(file_get_contents($yahoourl),true);
$tickermarket = str_replace('_',':',$data['dataset_code']); 
$company = $shortdata['COMPANY'];

$zack_eps = analyze('https://www.quandl.com/api/v3/datasets/ZEE/', $ticker .'_Q', $api_key);
$zack_zss = analyze('https://www.quandl.com/api/v3/datasets/ZSS/', $ticker, $api_key);
$zack_zea  = analyze('https://www.quandl.com/api/v3/datasets/ZEA/', $ticker, $api_key);
$zack_zar  = analyze('https://www.quandl.com/api/v3/datasets/ZAR/', $ticker, $api_key);
$zack_zes = analyze('https://www.quandl.com/api/v3/datasets/ZES/', $ticker, $api_key);


#var_dump($zack_eps);

$quarter1 = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][4]));
$quarter2 = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][5]));
$fy = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][6]));
$title =  "With Volume of" .$yahoo['query']['results']['quote']['AverageDailyVolume'] .", $company ($tickermarket) performance is mixed";
$keywords = <<< EX
{$tickermarket}, {$company}, {$company} analysis and updates, $tickermarket ratings, $company ratings, $company earnings expectations, $company analyst  ratings, $ticker earnings, $ticker earnings expectations
EX;

#echo $keywords;
/*** removed because we're not hitting yoast
$custom_fields = array(
                   array('key' => '_yoast_wpseo_newssitemap-stocktickers', 'value' => "$tickermarket"),
                   array('key' => '_yoast_wpseo_newssitemap-keywords', 'value' => $keywords)
                 );

   'terms_names'  => array( 
                'category' => array( 'business' )),
'custom_fields' => $custom_fields);

removed also because yahoo isn't working


***/

$params = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'post_title' => $title,
    'post_author' => 6,
    'post_excerpt' => '',
    'terms' => array('category' => array( 22 ) ) );



$params['post_content'] = <<<EX

What do Analysts think will be {$company}'s EPS?

Recently, we surveyed  {$zack_eps['dataset']['data'][0][5]} analysts were expecting an average of \${$zack_eps['dataset']['data'][0][1]} earnings per share  for {$company}, for the {$zack_eps['dataset']['data'][0][9]} quarter of the fiscal year ending
in {$zack_eps['dataset']['data'][0][8]}.<p>




<img src ="{$imgurl}">

{$company}'s graph of {$tickermarket} performance.

Today's market news:  {$company} trades against  a 52 week low of {$yahoo['query']['results']['quote']['YearLow']} and a 52 week of high {$yahoo['query'] ['results']['quote']['YearHigh']}. Technical indicators show a 50 day 
moving average of {$yahoo['query']['results']['quote']['FiftydayMovingAverage']}. In recent market movement, the  {$company} stock was seen at a {$yahoo ['query']['results']['quote']['ChangeFromFiftydayMovingAverage']} change from the 50 day moving average, which is 
{$yahoo['query']['results']['quote']['PercentChangeFromFiftydayMovingAverage']}.
<h1> Can {$company} be expected to surpass sales??</h1>


In its most recent quarter $company had actual sales of $ {$zack_zss[dataset][data][0][8]}M. Among the {$zack_zss[dataset][data][0][13]} analysts who were  surveyed, the consensus expectation for quarterly sales had been
{$zack_zss[dataset][data][0][10]}M. This represents a {$zack_zss[dataset][data][0][12]}% difference between analyst expectations and the {$company} achieved  in its quarterly earnings.



{$blurb}
</p>


<p>
 Among these  analysts, the highest expected EPS was \${$zack_eps['dataset']['data'][0][3]}. Counter to that,  the lowest was \${$zack_eps['dataset'] ['data'][0][4]}. This represents a {$zack_eps['dataset']['data'][0][7]}% change for the EPS reported for the same quarter in the prior year. This equates to  the consensus earnings growth estimate for the last 12 months, and should not be mistaken for a long term growth estimate.
There are a number of different data suppliers out there, so our  our reporting  may be different then the numbers reported by FactSet and other sources.





The bigger question, is, of course, can  ({$tickermarket}) shares  hit expected sales that analysts think it should get?

{$company} most recently announcied its earnings on {$zack_zes['dataset']['data'][0][0]}. After surveying {$zack_zes['dataset']['data'][0][7]} different  analysts, we established an average estimate of
$ {$zack_zes['dataset']['data'][0][1]} earnings per share and shares (EPS) for {$company} shares. Actual earnings for shares were last reported was  {$zack_zes['dataset']['data'][0][1]}.






</p>









<h2> Analyst Updates  for {$company} </h2>
Many esteemed, awesome, well known and loveable analysts are rating {$company}:

The overall rating for {$company} and shares is {$zack_zar['dataset']['data'][0][21]}.
 The rating is an average of the various different ratings given by analysts and brokers to {$company}, and then averaged into one rating by a team of  analysts at Zacks in Chicago, Illinois.
<p>
We've also learned that $company will report its next earnings on {$quarter1}. The earnings report after that one will be on {$quarter2}, and the report for  the fiscal year will be made available on {$fy}.
<br>
Last quarters actual earnings were {$zack_zea['dataset']['data'][0][10]} per share.
<br />
What are the estimates {$company}'s earnings? How well has {$company} actually performed?

<br>
Furthermore, {$company} exhibits  capitalization for its shares on the stock market of {$yahoo['query']['results']['quote']['MarketCapitalization']}.  {$company} Reported earnings before interest, taxes, debt and amortization (EBITDA) is {$yahoo['query']['results']['quote']['EBITDA']}. Earnings per share  (for all outstanding shares) were {$yahoo['query']['results']['quote']['EarningsShare']}.
</p>





















<p>






EX;

$res = $client-> query('wp.newPost',1, $usr, $pwd, $params);

echo $client->getResponse();

echo $params['post_content'];
echo $title;

}


}


    }
} else {
    echo "0 results";
}
$conn->close();
?> 