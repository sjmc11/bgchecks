<?php
$currentPage = 'selection'; // Set the current page to selection

include_once 'inc/functions.php'; // Calling the functions file
include_once 'inc/urls.php'; // Calling the URLs file

// Getting query string parameters and sanitizing them
$firstName = !empty($_GET['firstname']) ? sanitizeParameters($_GET['firstname']) : FALSE;
$lastName = !empty($_GET['lastname']) ? sanitizeParameters($_GET['lastname']) : FALSE;
$city = !empty($_GET['city']) ? sanitizeParameters($_GET['city']) : FALSE;
$state = !empty($_GET['state']) ? sanitizeParameters(strtoupper($_GET['state']), 'state') : 'ALL';
$address = !empty($_GET['address']) ? sanitizeParameters(substr($_GET['address'], 0, -1)) : FALSE;
$age = !empty($_GET['age']) ? sanitizeParameters($_GET['age']) : FALSE;
$total = !empty($_GET['total']) ? sanitizeParameters($_GET['total']) : FALSE;
$datasources = !empty($_GET['datasources']) ? sanitizeParameters($_GET['datasources']) : FALSE;
$fullName = FALSE;
$mapAddress = !empty($city) ? $city .', '. $state : $state;
$checkoutAppend = '';

// When mandatory fields are set assign first and last names to the fullName, otherwise redirect to the homepage
if(!empty($firstName) && !empty($lastName))
{
	$stateText = $state == 'ALL' ? 'All States' : $state;
	$forText = ' for '. ucfirst($firstName) .' '. ucfirst($lastName) .' in '. $stateText;
	$fullName = ucfirst($firstName) .' '. ucfirst($lastName);
	$onText = ' on <i>'. ucfirst($firstName) .' '. ucfirst($lastName) .'</i>';
	$onSpanText = ' on <span>'. ucfirst($firstName) .' '. ucfirst($lastName) .'</span>';
	$checkoutAppend .= '&searchfName='. $firstName .'&searchlName='. $lastName .'&searchState='. $state;
}

$metaTitle = 'Access all available records '. $fortext .' - BackgroundChecks.org';

include_once 'inc/header.php'; // Calling the header file
?>
<body>
<div id="container">
	<div id="exitOverlay"></div>
	<div class="lightbox2">
		<div class="lightbox2-content">
			<h2 class="lb2-title"><i>Wait</i>, Don't Leave!</h2>
			<h3 class="lb2-subtitle">Get Your Full Report<?= !empty($fullName) ? $onSpanText : ''; ?> for 100% <span>FREE</span></h3>
			<div class="lb2-limited">Please keep in mind that this is a limited time offer and may not be available in a couple of minutes!!</div>
			<div class="lb2-btn-block"><a class="btn" href="<?= $checkoutURL .'&items='. $prices['free']['items'] .'&sku='. $prices['free']['sku'] . $checkoutAppend; ?>">GET MY FREE REPORT</a></div>
			<div class="lb2-terms">You will receive your FREE REPORT + 5 DAYS UNLIMITED SEARCH PASS to run as many reports as you wish for <span>FREE</span></div>
			<div class="lb2-reports"></div>
		</div>
	</div>
	<?php
	include_once 'inc/headerMenu.php'; // Calling the header menu file
	?>

	<!-- Product Selection -->
	<div class="main">
		<div class="wrapper cf">
			<?php
			// Show page title only when fullName is set
			if(!empty($fullName))
			{
				?>
				<h1 class="page-title">Your Comprehensive Background Report<?= $onText; ?> is Ready!</h1>
				<?php
			}
			?>
			<div class="ps-content cf">
				<div class="half rgt">
					<div class="price-box">
						<div class="pb-header">Choose Access Level</div>
						<div class="pb-body">
							<div class="pb-body-top">
								<form action="<?= $checkoutURL; ?>" onsubmit="return checkInput();">
									<input type="hidden" name="merc_id" value="<?= $mercID; ?>">
									<input id="currentSKU" type="hidden" name="sku" value="<?= $prices['19.95']['sku']; ?>">

									<?php if(!empty($fullName)): ?>
									<input type="hidden" name="searchfName" value="<?= $firstName; ?>">
									<input type="hidden" name="searchlName" value="<?= $lastName; ?>">
									<input type="hidden" name="searchState" value="<?= $state; ?>">
									<?php endif; ?>

									<?php if(!empty($age)): ?>
									<input type="hidden" name="searchAge" value="<?= $age; ?>">
									<?php endif; ?>

									<?php if(!empty($address)): ?>
									<input type="hidden" name="searchLocation" value="<?= $address; ?>">
									<?php endif; ?>

									<?php if(!empty($datasources)): ?>
									<input type="hidden" name="searchSources" value="<?= $datasources; ?>">
									<?php endif; ?>
									
									<div class="ps-options">
										<div class="ps-option">
											<input id="option1" name="items" type="radio" value="<?= $prices['19.95']['items']; ?>" data-price="<?= $prices['19.95']['price']; ?>" checked>
											<label for="option1">
												<span class="ps-option-table">
													<span class="ps-option-cell">
														<span class="ps-option-title">1 Month Unlimited Full Access <span class="ps-option-best">Most Popular</span></span>
														<span class="ps-option-info">Report<?= $onText; ?> + 1 Month of Unlimited Reports</span>
													</span>
													<span class="ps-option-cell">
														<span class="ps-option-price">$19.95</span>
													</span>
												</span>
											</label>
										</div>
										<div class="ps-option">
											<input id="option2" name="items" type="radio" value="<?= $prices['4.95']['items']; ?>" data-price="<?= $prices['4.95']['price']; ?>">
											<label for="option2">
												<span class="ps-option-table">
													<span class="ps-option-cell">
														<span class="ps-option-title">5 Day Unlimited Access</span>
														<span class="ps-option-info">Report<?= $onText; ?> + 5 Days of Unlimited Reports</span>
													</span>
													<span class="ps-option-cell">
														<span class="ps-option-price">$4.95</span>
													</span>
												</span>
											</label>
										</div>
										<div class="ps-option">
											<input id="option3" name="items" type="radio" value="<?= $prices['29.95']['items']; ?>" data-price="<?= $prices['29.95']['price']; ?>">
											<label for="option3">
												<span class="ps-option-table">
													<span class="ps-option-cell">
														<span class="ps-option-title">Single Report Access</span>
														<span class="ps-option-info">Report<?= $onText; ?></span>
													</span>
													<span class="ps-option-cell">
														<span class="ps-option-price">$29.95</span>
													</span>
												</span>
											</label>
										</div>
									</div>
									<div class="pb-btn">
										<button class="form-btn" type="submit">View My Report</button>
									</div>
									<div class="pb-terms">
										<input id="check" name="check" type="checkbox" value="">
										<label for="check" id="disclaimer"><?= $disclaimer['19.95']; ?></label>
									</div>
								</form>
							</div>
							<div class="pb-secure">
								<div class="pb-secure-title">Secure Purchase</div>
								<div class="pb-secure-text">Keeping your information secure and confidential is our priority. Information submited here is encrypted using SSL, the most advanced technology available to prevent unauthorized access to your data.</div>
								<div class="pb-partners"><img src="img/ps_partners.png" alt=""></div>
							</div>
						</div>
					</div>
				</div>
				<div class="half lft">
					<div class="ps-block">
						<?php
						// Show this block only when fullName is set
						if(!empty($fullName))
						{
							$ageText = !empty($age) ? '<li><span>Age:</span> '. $age .'</li>' : '';
							$locationText = !empty($address) ? '<li><span>Location:</span> '. str_replace('|', ' / ', $address) .'</li>' : '';
							$sourcesText = !empty($datasources) ? '<li><span>Data Sources Searched:</span> '. $datasources .'</li>' : '';
							$totalText = !empty($total) ? '<li><span>Records Found:</span> '. $total .'</li>' : '';
							?>
							<div class="ps-sum">
								<div class="ps-sum-table">
									<div class="cell ps-sum-photo"><img src="img/ps_nophoto.png" alt=""></div>
									<div class="cell ps-sum-info">
										<div class="ps-sum-name"><?= $fullName; ?></div>
										<div class="ps-sum-list">
											<ul>
												<?= $ageText; ?>
												<?= $locationText; ?>
											</ul>
											<ul>
												<li><span>Comprehensive Background Report:</span> <i>Ready</i></li>
												<?= $sourcesText; ?>
												<?= $totalText; ?>
												<li><span>Report Date:</span> <i><?= date('F j, Y'); ?></i></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<?php
						}
						?>
						<div class="ps-map"><div id="map_canvas"></div></div>
					</div>
				</div>
				<div class="half rgt mobile">
					<div class="ps-block">
						<h3 class="ps-block-title">Report Will Include:</h3>
						<div class="ps-incs cf">
							<ul>
								<li>Arrest/Warrant Records</li>
								<li>Criminal Records</li>
								<li>Criminal Driving Violations</li>
								<li>Jail and Inmate Records</li>
								<li>Conviction Records</li>
								<li>Sex Offender Records</li>
								<li>Felony and Conviction Records</li>
								<li>Bankruptcies and Liens</li>
								<li>Civil Judgments</li>
								<li>Lawsuits</li>
								<li>Marriage Records</li>
								<li>Divorce Records</li>
								<li>Birth Records</li>
								<li>Property Records</li>
								<li>Asset Records</li>
								<li>Address History</li>
								<li>Phone Numbers</li>
								<li>Emails and Social Profiles</li>
								<li>Relatives and Associates</li>
								<li>Business Ownership</li>
								<li>Income and Education Info</li>
								<li>Sentencing Files</li>
								<li>Incarceration Records</li>
								<li>Correctional Files</li>
								<li>DUI/DWI Records</li>
								<li>Speeding Tickets</li>
								<li>Reckless Driving Charges</li>
								<li>Bail/Parole Details</li>
								<li>Conviction Records</li>
								<li>Disposition Details</li>
								<li>Arresting Agency</li>
								<li>Police Report</li>
								<li>Ancestry Records</li>
								<li>Auto Ownership Records</li>
								<li>Vessel/Aircraft Ownership</li>
								<li>Business Ownership Records</li>
								<li>Net Worth Information</li>
								<li>Unclaimed Money Records</li>
								<li>Occupation</li>
								<li>And More!</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="half lft">
					<div class="ps-block">
						<h3 class="ps-block-title">Frequently Asked Questions</h3>
						<div class="ps-faqs">
							<div class="ps-faq">
								<h4>How is the data compiled for these reports?</h4>
								<p>We collect and index billions of records from over 3,000 public and private sources and bring you one comprehensive report on any individual in the United States.</p>
							</div>
							<div class="ps-faq">
								<h4>How many reports can I run?</h4>
								<p>If you choose our Unlimited plan which is the most popular one you will be able to run as many reports as you like as long as your subscription is active. There are absolutely no restrictions, search anyone and anytime and any number of times. You receive full unrestricted unlimited access to all our records, all 2 billion of them!</p>
							</div>
							<div class="ps-faq">
								<h4>How Quickly Will I Access My Search Results?</h4>
								<p>All search results are accessed instantly. Right after you complete this step you will gain access to your search results.</p>
							</div>
							<div class="ps-faq">
								<h4>Will My Test Subject Be Notified About This Report?</h4>
								<p>Every search you make within our system is highly confidential, your test subject will never know that you are running this report.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="half rgt desktop">
					<div class="ps-block">
						<h3 class="ps-block-title">Report Will Include:</h3>
						<div class="ps-incs cf">
							<ul>
								<li>Arrest/Warrant Records</li>
								<li>Criminal Records</li>
								<li>Criminal Driving Violations</li>
								<li>Jail and Inmate Records</li>
								<li>Conviction Records</li>
								<li>Sex Offender Records</li>
								<li>Felony and Conviction Records</li>
								<li>Bankruptcies and Liens</li>
								<li>Civil Judgments</li>
								<li>Lawsuits</li>
								<li>Marriage Records</li>
								<li>Divorce Records</li>
								<li>Birth Records</li>
								<li>Property Records</li>
								<li>Asset Records</li>
								<li>Address History</li>
								<li>Phone Numbers</li>
								<li>Emails and Social Profiles</li>
								<li>Relatives and Associates</li>
								<li>Business Ownership</li>
								<li>Income and Education Info</li>
								<li>Sentencing Files</li>
								<li>Incarceration Records</li>
								<li>Correctional Files</li>
								<li>DUI/DWI Records</li>
								<li>Speeding Tickets</li>
								<li>Reckless Driving Charges</li>
								<li>Bail/Parole Details</li>
								<li>Conviction Records</li>
								<li>Disposition Details</li>
								<li>Arresting Agency</li>
								<li>Police Report</li>
								<li>Ancestry Records</li>
								<li>Auto Ownership Records</li>
								<li>Vessel/Aircraft Ownership</li>
								<li>Business Ownership Records</li>
								<li>Net Worth Information</li>
								<li>Unclaimed Money Records</li>
								<li>Occupation</li>
								<li>And More!</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="half lft">
					<div class="ps-reviews block cf">
						<div>
							<div class="ps-review">I am a retired police officer and let me tell you that the information that you have you could only access if you were in law enforcement. This is truly revolutionary that people can check criminal records online.</div>
							<div class="ps-author">Frank S, Toledo, OH</div>
						</div>
						<div>
							<div class="ps-review">To be honest I am always skeptical of these investigative sites on the net and I was duped a few times. Your site has real records and not a bunch of links to other sites.</div>
							<div class="ps-author">Victor T, Wilmington, NC</div>
						</div>
						<div>
							<div class="ps-review">I am on the Board for our State Wide Private Investigator Association (PIAU). I have recommended your site and services to all our members.</div>
							<div class="ps-author">Alan P, Los Angeles, CA</div>
						</div>
						<div>
							<div class="ps-review">Looking through this database showed me the truth about my potential business partner. I am so glad that I found it before I went any deeper. Thanks a bunch!</div>
							<div class="ps-author">Laura R, Brooklyn, NY</div>
						</div>
					</div>
				</div>
				<div class="half rgt">
					<div class="ps-verified">We make sure to provide you only with the most up-to-date and accurate information. In the event you are not fully satisfied, please contact us for fast customer service and expert support.</div>
				</div>
			</div>
		</div>
	</div>
	<!-- // Product Selection -->

<?php
include_once 'inc/footer.php'; // Calling the footer file
?>