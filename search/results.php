<?php
$currentPage = 'teaser'; // Set the current page to teaser
$hideOptions = TRUE; // Setting TRUE to hide the options tags
include_once 'inc/USstates.php'; // Include US states
include_once 'inc/functions.php'; // Calling the functions file

// Getting query string parameters and sanitizing them
$firstName = !empty($_GET['firstname']) ? sanitizeParameters($_GET['firstname']) : FALSE;
$lastName = !empty($_GET['lastname']) ? sanitizeParameters($_GET['lastname']) : FALSE;
$state = !empty($_GET['state']) ? sanitizeParameters(strtoupper($_GET['state']), 'state') : 'ALL';

// Checking if mandatory fields are set, else redirect to home
if(!empty($firstName) && !empty($lastName))
{
	$totalRows = 0;
	$fullName = ucfirst($firstName) .' '. ucfirst($lastName);
	$results = getNameResults($firstName, $lastName, $state); // Get results
	$stateText = $state == 'ALL' ? 'Nationwide' : $states[$state];
	$loader2URLGlobal = $loader2URL .'?firstname='. $firstName .'&lastname='. $lastName .'&state='. $state;

	if(!empty($results))
	{
		include_once 'inc/countiesTotals.php'; // Calling the counties total number array file

		$dataSources = number_format($countiesTotals[$state]);
		$totalRows = $results->total_rows;
		$loader2URLGlobal .= '&datasources='. $dataSources .'&total='. $totalRows;
	}
}else
{
	header('location: '. $baseURL);
	exit;
}

$metaTitle = 'Search results for '. $fullName .' in '. $stateText .' - BackgroundChecks.com';

include_once 'inc/header.php'; // Calling the header file
?>
<body>

<div id="container">
	<?php
	include_once 'inc/headerMenu.php';

	if(!empty($results))
	{
		?>
		<!-- Results -->
		<div class="results-intro">
			<div class="wrapper cf">
				<div class="ri-table cf">
					<div class="cell">
						<h1 class="ri-title">Search Successful! Found <?= $totalRows; ?> Reports</h1>
						<p class="ri-text"><strong>Next Step:</strong> Find the result below that best matches your search criteria. If you are not sure you can view all <?= $totalRows; ?> reports for no additional charge</p>
						<div class="ri-btn"><a class="btn btn-big arw" href="<?= $loader2URLGlobal; ?>">View All Reports</a></div>
					</div>
					<div class="cell">
						<div class="ri-sum">
							<h2>Search Summary:</h2>
							<ul>
								<li>Data Sources Searched: <span><?= $dataSources; ?></span></li>
								<li>Database Last Update: <span><?= date('F j, Y'); ?></span></li>
								<li>Subject Name: <span class="special"><?= $fullName; ?></span></li>
								<li>State: <span><?= $stateText; ?></span></li>
								<li>Search Results: <span>Compiled <?= $totalRows; ?> Reports</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="results-content">
			<div class="wrapper cf">
				<div class="results-note">
					<ul>
						<li class="icn-01">This search his 100% confidential, <?= $fullName; ?> will not be notified</li>
						<li class="icn-02"><i>REAL TIME DATA</i> - Information below comes from county, state and federal sources</li>
					</ul>
				</div>
				<div class="r4-table">
					<table class="tablesorter">
						<thead>
							<tr>
								<th class="name">Name:</th>
								<th class="location">Location:</th>
								<th class="sources">Data Sources:</th>
								<th class="verified">Verified Report:</th>
								<th class="button">Open Report:</th>
							</tr>
						</thead>
						<tbody>
						<?php
						// Looping through the profiles
						foreach($results->recordset->record as $profile)
						{
							$profileFirstName = !empty($profile->firstname) ? $profile->firstname : ''; // Profile first name
							$profileLastName = !empty($profile->lastname) ? $profile->lastname : ''; // Profile last name
							$profileFullName = $profileFirstName .' '. $profileLastName; // Profile full name
							$profileAge = !empty($profile->age) ? $profile->age : 'NA'; // Profile age
							$profileFullName .= $profileAge != 'NA' ? ', '. $profileAge : '';
							$profileState = $state; // Currently assigning to the main state
							$profileCity = '';
							$toLoader2URL = $loader2URL .'?firstname='. $profileFirstName .'&lastname='. $profileLastName; // Building current profile href

							$addresses = '';
							$addressesToLoader = '';
							$sources = '';
							$addFlag = 0;

							// Looping through the previous addresses
							foreach ($profile->addresses->address as $address)
							{
								if($addFlag == 0)
								{
									$profileState = $address->state; // Changing state to the current profile first state
									$profileCity = $address->city;
									$toLoader2URL .= '&city='. $profileCity; // Appending the city to the second loader URL
								}

								// When previous addresses exceed 3 then add a link and break the loop
								if($addFlag == 3)
								{
									$addresses .= '<a href="'. $toLoader2URL .'&state='. $profileState .'">More Results</a>';
									break; // Break if previous addresses are more then 3
								}

								$addressesToLoader .= $address->city .', '. $address->state .'|';
								$addresses .= $address->city .', '. $address->state .'</br>';
								$addFlag++;
							}

							$sources = getSources($profileCity, $profileState);

							$toLoader2URL .= '&state='. $profileState .'&address='. $addressesToLoader .'&age='. $profileAge .'&datasources='. $dataSources; // Appending the state, address, age, sources count to the second loader URL

							// Printing profiles
							echo '<tr>
									<td class="name"><a href="'. $toLoader2URL .'">'. $profileFullName .'</a></td>
									<td class="location">'. $addresses .'</td>
									<td class="sources">'. $sources .'</td>
									<td class="verified"><div class="icn-verified"></div></td>
									<td class="button"><a class="btn btn-med arw" href="'. $toLoader2URL .'">Open Report</a></td>
								</tr>';
						}
						?>
						</tbody>
					</table>
				</div>
				<div class="results-search">Can't Find Who You're Looking For? <a href="<?= $loader2URLGlobal; ?>">Search Our Entire Database Here</a></div>
			</div>
		</div>
		<!-- // Results -->
		<?php
	}else // No hit case
	{
		include_once 'nohit.php'; // Load no hit page
	}

include_once 'inc/footer.php'; // Calling the footer file
?>