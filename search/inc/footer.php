	<!-- Footer -->
	<footer id="footer">
		<div class="wrapper cf">
			<div class="fcopy"><strong>Copyright <?= date('Y'); ?> by Background Checks.org.</strong> Background Checks.org is operated as a public service to individuals. We are not a consumer reporting agency as defined by the Fair Credit Reporting Act, and you may not use any information for any "permitted purpose".</div>
		</div>
	</footer>
	<!-- // Footer -->

	<?php
	include_once 'googleAnalytics.php'; // Calling google analytics
	?>

</div>

<!-- jQuery -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

<!-- Slick Nav -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
<script src="js/slicknav/jquery.slicknav.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#menu').slicknav();
	});
</script>
<?php
// First loader functionality
if($currentPage == 'loader'):
	?>
	<!-- Loader list functionality -->
	<script type="text/javascript">
		function shuffle(o){
			for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		}

		$(document).ready(function(){
			var liCount = $('#loader-list li').size();
			var liOrdering = []; // Creating the li ordering array
			var circleIndex = [7, 15, 19, 27, 29, 31, 41, 48, 56, 66, 73, 78, 81, 87, 99]; // When a particular index should be marked
			currentPercent = 0; // Starting progressbar percent

			// Adding indexes to the array
			for (var i = 1; i <= 15; i++) {
				liOrdering.push(i);
			}

			shuffle(liOrdering); // Shuffle for a random order

			var interval = setInterval(progressbar, 100); // 10 Seconds overall progressbar

			function progressbar(){
				// While current percent is less than 100, otherwise clear interval and change percent text
				if(currentPercent < 101){
					if(currentPercent == circleIndex[0]){
						$('#loader-list li:nth-child('+ liOrdering[0] +') i.loader-circle').remove(); // Removing the loader circle
						liOrdering.shift(); // Removing the 0 element from the li ordering array
						circleIndex.shift(); // Removing the 0 element from the index array
					}

					$('.loader-progress div').width(++currentPercent +'%'); // Adding the width of the progressbar
				}else{
					clearInterval(interval);
					window.location.href = "<?= $teaserURL; ?>"; // Redirect to the teaser page
				}
			}
		});
	</script>
	<?php
endif;

// Second loader functionality
if($currentPage == 'loader2'):
	?>
	<!-- Loader list functionality -->
	<script type="text/javascript">
		function shuffle(o){
			for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		}

		$(document).ready(function(){
			var circleIndex = [27, 48, 81]; // When a particular index should be marked
			currentPercent = 0; // Starting progressbar percent

			var interval = setInterval(progressbar, 50); // 5 Seconds overall progressbar

			function progressbar(){
				// While current percent is less than 100, otherwise clear interval and change percent text
				if(currentPercent < 101){
					if(currentPercent == circleIndex[0]){
						$('#circle1').remove(); // Removing the loader circle
					}else if(currentPercent == circleIndex[1]){
						$('#circle2').remove(); // Removing the loader circle
					}else if(currentPercent == circleIndex[2]){
						$('#circle3').remove(); // Removing the loader circle
					}

					$('.loader-progress div').width(++currentPercent +'%'); // Adding the width of the progressbar
				}else{
					clearInterval(interval);
					window.location.href = "<?= $selectionURL; ?>"; // Redirect to the teaser page
				}
			}
		});
	</script>
	<?php
endif;

// Product Selection functionality
if($currentPage == 'selection'):
	?>
	<!-- Reviews -->
	<link href="js/slick/slick.css" rel="stylesheet" type="text/css">
	<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<script src="js/slick/slick.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.ps-reviews').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 5000,
			});
		});
	</script>
	<script src="//maps.googleapis.com/maps/api/js?v=3.exp&&key=AIzaSyAM0ouXRF5M3n9bpsz38cB7dNagt_rmhWE&libraries=places&language=en&region=US"></script>
	<script>
	var geocoder;
	var map;
	var address ="<?= $mapAddress; ?>"; // The address that needs to be zoom the marker on it

	function initialize(){
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(35.029996, -99.316406); // US center lat lon

		var myOptions = {
			zoom: 4,
			center: latlng,
			zoomControl: false,
			streetViewControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

		if (geocoder) {
			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
						map.setCenter(results[0].geometry.location);
						map.setZoom(10);

						var marker = new google.maps.Marker({
							position: results[0].geometry.location,
							map: map,
							title:address,
							icon: '/img/co2_map_icn.png'
						});
					}
				}
			});
		}
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<script type="text/javascript">
	// Removing the exit unload, this is for the form or paypal subumission cases
	function cleanOnBeforeUnload(){
		window.onbeforeunload = '';
		return true;
	}

	$(function() {
		$('body a, :submit').click(function() {
			cleanOnBeforeUnload(); // Clean onbeforeunload
		});
	});

	$(document).ready(function() {
		window.onbeforeunload = exitAlert;

		// Show the exit lightbox
		function exitAlert(){
			$("html, body").scrollTop(0);

			$("#exitOverlay, .lightbox2").show(); // Show the exit lightbox
			$("body").css('overflow', 'hidden');

			cleanOnBeforeUnload();

			// Mozilla firefox
			if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
				return false;
			}else{
				// For most used browsers, except FF
				return 'Wait, don\'t leave! Take advantage of our limited time offer';
			}
		}
	});

	function checkInput( ){
		if($(".pb-terms").is(":visible") == true)
		{
			if( $('#check:checked').length > 0 ){
				return true;
			}else{
				alert('Please agree to the Terms of Service to continue.');
				return false;
			}
		}
	}

	$(function(){
		var prices = new Array('19.95', '4.95', '29.95');
		prices['19.95'] = new Array('items', 'sku', 'rebill', 'disclaimer');
		prices['4.95'] = new Array('items', 'sku', 'rebill', 'disclaimer');
		prices['29.95'] = new Array('items', 'sku', 'rebill');

		prices['19.95']['items'] = '<?= $prices['19.95']['items']; ?>';
		prices['19.95']['sku'] = '<?= $prices['19.95']['sku']; ?>';
		prices['19.95']['rebill'] = '<?= $prices['19.95']['rebill'] ? 'true' : 'false'; ?>';
		prices['19.95']['disclaimer'] = '<?= $disclaimer['19.95']; ?>';

		prices['4.95']['items'] = '<?= $prices['4.95']['items']; ?>';
		prices['4.95']['sku'] = '<?= $prices['4.95']['sku']; ?>';
		prices['4.95']['rebill'] = '<?= $prices['4.95']['rebill'] ? 'true' : 'false'; ?>';
		prices['4.95']['disclaimer'] = '<?= $disclaimer['4.95']; ?>';

		prices['29.95']['items'] = '<?= $prices['29.95']['items']; ?>';
		prices['29.95']['sku'] = '<?= $prices['29.95']['sku']; ?>';
		prices['29.95']['rebill'] = '<?= $prices['29.95']['rebill'] ? 'true' : 'false'; ?>';

		var currentPrice = $('input[name="items"]:checked').data('price');
		var currentIndex = currentPrice.slice(1);

		if(prices[''+ currentIndex +'']['rebill'] == 'true'){
			$('.pb-terms').show();
			$('#disclaimer').text(prices[''+ currentIndex +'']['disclaimer']); // Updating the disclaimer text
		}else{
			$('.pb-terms').hide();
		}

		$('input[type="radio"]').click(function(){
			var dataPrice = $(this).data('price'); // Getting data attribute value (e.g. $19.95)
			var currentPrice = dataPrice.slice(1); // Removing the $ sign (e.g. 19.95)
			var currentSKU = prices[''+ currentPrice +'']['sku']; // Getting current SKU based on the attr value
			$('#currentSKU').val(currentSKU); // Updating current SKU

			if(prices[''+ currentPrice +'']['rebill'] == 'true'){
				$('.pb-terms').show();
				$('#disclaimer').text(prices[''+ currentPrice +'']['disclaimer']); // Updating the disclaimer text
			}else{
				$('.pb-terms').hide();
			}
		});
	});
    </script>
	<?php
endif;
?>
</body>
</html>