<?php
include_once 'urls.php'; // Calling the URLs file

// This function is mainly for sanitizing GET/POST parameters against XSS injection
function sanitizeParameters($value, $type = FALSE)
{
	// Checking if value is set then sanitize and return it, otherwise return false
	if(!empty($value))
	{
		$value = strip_tags($value); // Removing HTML and PHP tags from a string if any

		return filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS); // Filtering/Sanitizing from special characters before returning
	}elseif(empty($value) && $type == 'state')
	{
		// Return nationwide state when the state is empty
		return 'ALL';
	}
	else
	{
		return FALSE;
	}
}

// This function creates a cookie
function createCookie($name, $value, $expireDay = 90)
{
	$expire = time() + (3600 * 24 * $expireDay);
	setcookie($name, $value, $expire, '/', '.inteligator.com');
	$_COOKIE[$name] = $value;
}

// Get the users real IP address
function getClientIP()
{
	$ipaddress = '';

	if(getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
	else if(getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	else if(getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
	else if(getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	else if(getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
	else if(getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
	else
		$ipaddress = FALSE;

	return urlencode($ipaddress);
}

// curlContent function to get the content of the URL
function getCurlContent($url, $timeout = 90)
{
	// Get the XML via cURL
	$ch	 = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // For https cases
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); // The maximum number of seconds to allow cURL functions to execute.
	curl_setopt($ch, CURLOPT_URL, $url); // Get the url contents
	$data = curl_exec($ch); // Execute cURL request

	// Check for cURL errors
	if(curl_errno($ch) !== 0 || empty($data))
	{
		return FALSE;
	}

	curl_close($ch);
	return $data;
}

// Get the name results based on XML URL if any, otherwise return false
function getNameResults($firstName, $lastName, $state)
{
	global $xmlURL;

	// Checking if mandatory parameters are not empty
	if(!empty($firstName) && !empty($lastName) && !empty($state))
	{
		$firstName = str_replace(' ', '+', $firstName); // Replacing whitespace with + sign
		$lastName = str_replace(' ', '+', $lastName); // Replacing whitespace with + sign
		$XMLqueryString = '&fname='. $firstName .'&lname='. $lastName .'&state='. $state;

		// Building name search XML URL
		$xmlURL .= $XMLqueryString;

		// Get profiles from XML URL
		$profiles = getCurlContent($xmlURL, 30);

		// Interpreting a string of output into an object and assigning to $xml
		$xml = simplexml_load_string($profiles);

		// Check if XML returned no results then try the backup XML
		if(!$xml || $xml->total_rows == 0)
		{
			return FALSE;
		}

		return $xml;
	}
	else
	{
		return FALSE;
	}
}

// Get the sources based on city and state in XML URL if any, otherwise return false
function getSources($city = FALSE, $state = FALSE)
{
	global $sourcesURL;

	// Checking if mandatory parameters are not empty
	if(!empty($city) && !empty($state))
	{
		$finalResults = '';
		$city = str_replace(' ', '+', $city); // Replacing whitespace with + sign
		$XMLqueryString = '&city='. $city .'&state='. $state;

		// Building name search XML URL
		$sourcesURL .= $XMLqueryString;

		// Get profiles from XML URL
		$profiles = getCurlContent($sourcesURL, 30);

		// Interpreting a string of output into an object and assigning to $xml
		$xml = simplexml_load_string($profiles);

		// Check if XML returned no results then try the backup XML
		if(!$xml)
		{
			return FALSE;
		}else
		{
			$results = $xml->recordset;

			$policeStation = !empty($results->policeStation->name) ? $results->policeStation->name : '';
			$sheriffOffice = !empty($results->sheriffOffice->name) ? $results->sheriffOffice->name : '';
			$courts = !empty($results->courts->name) ? $results->courts->name : '';

			$finalResults = $policeStation .'<br />'. $sheriffOffice .'<br />'. $courts;
		}

		return $finalResults;
	}
	else
	{
		return FALSE;
	}
}