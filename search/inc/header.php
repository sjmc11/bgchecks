<?php
$metaTitle = !empty($metaTitle) ? $metaTitle : 'BackgroundChecks.org';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?= $metaTitle; ?></title>
	<link href="<?= $homeURL; ?>img/favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
	<link href="<?= $homeURL; ?>css/style.css" rel="stylesheet" type="text/css">
	<link href="<?= $homeURL; ?>css/responsive.css" rel="stylesheet" type="text/css">
</head>