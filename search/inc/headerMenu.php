<!-- Header -->
<header id="header">
	<div class="wrapper cf">
		<div class="site-logo"><a href="<?= $baseURL; ?>">BackgroundChecks.org</a></div>
		<nav id="menu" class="hnav">
			<ul>
				<li><a href="<?= $loginURL; ?>">Login</a></li>
				<li><a href="<?= $contactURL; ?>">Contact Us</a></li>
				<li><a href="<?= $newsRoomURL; ?>">Newsroom</a></li>
			</ul>
		</nav>
	</div>
</header>
<!-- // Header -->