<?
// Assigning US states to the array
$states = array(
	'AL' => 'Alabama',
	'AK' => 'Alaska',
	'AR' => 'Arkansas',
	'AZ' => 'Arizona',
	'CA' => 'California',
	'CO' => 'Colorado',
	'CT' => 'Connecticut',
	'DC' => 'District of Columbia',
	'DE' => 'Delaware',
	'FL' => 'Florida',
	'GA' => 'Georgia',
	'HI' => 'Hawaii',
	'IA' => 'Iowa',
	'ID' => 'Idaho',
	'IL' => 'Illinois',
	'IN' => 'Indiana',
	'KS' => 'Kansas',
	'KY' => 'Kentucky',
	'LA' => 'Louisiana',
	'MA' => 'Massachusetts',
	'MD' => 'Maryland',
	'ME' => 'Maine',
	'MI' => 'Michigan',
	'MN' => 'Minnesota',
	'MO' => 'Missouri',
	'MS' => 'Mississippi',
	'MT' => 'Montana',
	'NE' => 'Nebraska',
	'NH' => 'New Hampshire',
	'NJ' => 'New Jersey',
	'NM' => 'New Mexico',
	'NV' => 'Nevada',
	'NC' => 'North Carolina',
	'ND' => 'North Dakota',
	'NY' => 'New York',
	'OH' => 'Ohio',
	'OK' => 'Oklahoma',
	'OR' => 'Oregon',
	'PA' => 'Pennsylvania',
	'RI' => 'Rhode Island',
	'SD' => 'South Dakota',
	'SC' => 'South Carolina',
	'TN' => 'Tennessee',
	'TX' => 'Texas',
	'UT' => 'Utah',
	'VA' => 'Virginia',
	'VT' => 'Vermont',
	'WA' => 'Washington',
	'WI' => 'Wisconsin',
	'WV' => 'West Virginia',
	'WY' => 'Wyoming'
);

// Check if nationwideState flag was set then add in the beginning of the array the nationwide state option
if(!empty($nationwideState))
{
	$nationwide = array('ALL' => 'United States');
	$states = $nationwide + $states;
}

if(!isset($hideOptions))
{
	foreach($states as $abbr => $state)
	{
		?><option value="<?= $abbr; ?>"<?= strtolower($REQ['state']) == strtolower($abbr) ? ' selected="selected"' : ''; ?>><?= !empty($fullState) ? $state : $abbr; ?></option><?
	}
}