<!-- Nohit -->
<div class="nohit2">
	<div class="wrapper cf">
		<div class="nohit2-header">We were unable to find exact results for your search in our preliminary database.</div>
		<div class="nohit2-body">
			<h2 class="nohit2-title"><i>Expand Your Search</i> - Try BackgroundChecks.org Premium Unlimited Access</h2>
			<div class="nohit2-container cf">
				<div class="nohit2-sidebar">
					<div class="nohit2-report">
						<div class="nohit2-report-name"></div>
					</div>
					<div class="nohit2-btn-block"><a class="btn btn-big arw" href="<?= $selectionURL; ?>">Register to Access All Records</a></div>
				</div>
				<div class="nohit2-content">
					<div class="nohit2-content-inn">
						<div class="nohit2-record icn-01">
							<h3>Unlimited Criminal Records Lookups</h3>
							<p>Search for Arrest Records, Warrants, Felony Records, Convictions, Misdemeanors, Police Records, Jail Records And More!</p>
						</div>
						<div class="nohit2-record icn-02">
							<h3>Unlimited Court Case Lookups</h3>
							<p>Search for Bankruptcy Records, Tax &amp; Property Liens, Legal Judgments, Lawsuits, Speeding Tickets and Much More!</p>
						</div>
						<div class="nohit2-record icn-03">
							<h3>Unlimited Vital Records Lookups</h3>
							<p>Search for Marriage, Divorce, Birth and Death Records</p>
						</div>
						<div class="nohit2-record icn-04">
							<h3>Unlimited Asset Lookups</h3>
							<p>Search for Property Ownership Records, Auto &amp; Aircraft Ownership, Boat Ownership Records, Business Ownership and Much More!</p>
						</div>
						<div class="nohit2-record icn-05">
							<h3>Unlimited People Lookups</h3>
							<p>Search for Addresses, Phone Numbers, Income Information, Education, Relatives, Aliases, Social Profiles and Much More!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- // Nohit -->